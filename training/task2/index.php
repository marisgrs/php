<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <p>hello task2</p>
    <body>
        <?php
        /*------------------------------------------------*/
        echo '<br>Урок 3: Типы данных  <br>';
        /*------------------------------------------------*/
        echo '<br>Задача 1: Типы переменных <br>';
        $a = 152; 
        $b = '152'; 
        $c = 'London';
        $d = array(152); 
        $e = 15.2; 
        $f = false; 
        $g = true;
        echo '$a=' . gettype($a) . '</br>';
        echo '$a=' . gettype($b) . '</br>';
        echo '$a=' . gettype($c) . '</br>';
        echo '$a=' . gettype($d) . '</br>';
        echo '$a=' . gettype($e) . '</br>';
        echo '$a=' . gettype($f) . '</br>';
        echo '$a=' . gettype($g) . '</br>';
        /*------------------------------------------------*/
        echo '<br>Задача 2: Работа со строками и переменными <br>';        
        $a = 10; 
        $b = 5;
        echo $b . ' из ' . $a . 'ти студентов посетило лекцию.' . "</br>";
        echo '<br>Задача 3: Работа со строками и переменными<br>';
        $t1 = 'Доброе утро';
        $t2 = 'дамы';
        $t3 = 'и господа';
        echo  $t1 . ' '  . $t2 . ' '  . $t3 .   '</br>';
        /*------------------------------------------------*/
        echo '<br>Задача 1: Комментарии<br>';
        $arr1 = array('1', '2' , '3', '4' , '5'); 
        $arr2 = array('10', '20' , '30', '40' , '50'); 
        $arr1[1] = '200';
        echo '$arr1[1]=' . $arr1[1] . '</br>';
        unset($arr2[0]);
        echo '$arr1[0]=' . $arr1[0] . '</br>';
        echo '$arr2[0]=' . $arr2[0] . '</br>';
        echo '$arr1 count=' . count($arr1) . '</br>';
        echo '$arr2 count=' . count($arr2) . '</br>';
         foreach ($arr1 as $value) {
             echo "$value <br>";
        }
        foreach ($arr2 as $value) {
             echo "$value <br>";
        }
        /*------------------------------------------------*/       
        echo '<br>Урок 4: Условный оператор<br>';
        /*------------------------------------------------*/
        echo '<br>Задача 1: Вхождение числа в диапазон<br>';
        define('MIN', 10);
        define('MAX', 50);
        $x = 50;
        if ($x < MIN || $x > MAX){
            echo 'x = ' .'-' . '<br>';  
        }elseif ($x == MIN || $x == MAX) {
            echo 'x = ' . '+-' . '<br>'; 
        } else {
            echo 'x = ' .'+' . '<br>'; 
        }
        
        if ($x > MIN && $x < MAX){
            echo 'x = ' .'+' . '<br>';  
        }elseif ($x == MIN || $x == MAX) {
            echo 'x = ' . '+-' . '<br>'; 
        } else {
            echo 'x = ' .'-' . '<br>'; 
        }
        /*------------------------------------------------*/
        echo '<br>Задача 2: Квадратное уравнение<br>';
        $a = 1;
        $b = 2;
        $c = 1;
        $x1 = 0;
        $x2 = 0;
        $d = (pow($b, 2))-(4 * $a * $c);
        if ($d > 0){
            $x1 = (-$b + sqrt($d))/(2 * $a);
            $x2 = (-$b - sqrt($d))/(2 * $a);
        } elseif ($d == 0 ) { 
            $x1 = (-$b /(2 * $a));
            $x2 = $x1; 
        } else {
            $x1 = 'решения нет';
            $x2 = $x1;
        }
        echo 'd = ' .  $d . '<br>'; 
        echo 'x1 = ' .  $x1 . '<br>';  
        echo 'x2 = ' .  $x2 . '<br>';  
        /*------------------------------------------------*/
        echo '<br>Задача 3: Равенство чисел<br>';
        $a = 1;
        $b = 3;
        $c = 2;
        $arr = array();
        $arr[0] = $a;
        $arr[1] = $b;
        $arr[2] = $c;
        sort($arr);
        $x = $arr[count($arr)-1];
        foreach ($arr as $value) {
            if ($x == $value) {
                $s = 'ошибка';
                break;
            } else {
                $x = $value;
            }
        }
        if ($s != 'ошибка') {
            $s = $arr[(count($arr)-1) / 2];
        }
        echo  'среднее число  = '. $s . '<br>';
        /*------------------------------------------------*/
        echo '<br>Задача 3: Равенство чисел Вар2 <br>';
        if ($a !== $b & $a !== $c & $c != $b){
            if ((($b > $a) & ($a > $c)) || (($c > $a) & ($a > $b))) {
                $s = $a;
            } elseif((($a > $b) & ($b > $c)) || (($c > $b) & ($b > $a))){
                $s = $b;
            } else{
                $s = $c;
            }
        } else{
            $s = 'ошибка';
        }
        echo  'среднее число  = '. $s . '<br>';
        /*------------------------------------------------*/
        echo '<br>Урок 5: Циклы <br>';
        /*------------------------------------------------*/
        echo '<br>Задача 1: Сумма чисел<br>';
        for ($x = 0; $x <= 25; $x++) {
             $s1 += $x;
            } 
        echo  'сумма  = '. $s1 . '<br>';
        $x = 0;
        while ( $x <= 25) {
             $s2 += $x;
             $x++;
            } 
        echo  'сумма  = '. $s2 . '<br>';
        /*------------------------------------------------*/
        echo '<br>Задача 2: Квадраты чисел<br>';
        $n = 125;
        $x = 1;
        $sq = 0;
        while ( $n > $sq) {
             $sq = pow($x, 2);   
             $x++;
             echo  'число  = ' . $x  . ' корень = ' . $sq . '<br>';
        }
        /*------------------------------------------------*/
        echo '<br>Урок 5: Циклы <br>';
        /*------------------------------------------------*/
        echo '<br>Задача 3: Меню на сайте<br>';
            $buttonX1 = '<li><a href="#">Кнопка ';
            $buttonX3 = '</a></li> ';

           $arr = array();
           for ($x = 0 ; $x < 10 ; $x++){
               $arr[$x] = $x+1;
           }

           sort($arr);
           echo  '<ul>';
           foreach ($arr as  $buttonX2 ){
             echo  $buttonX1 . $buttonX2 . $buttonX3;
           }
           echo '</ul>'; 

           rsort($arr);
           echo  '<ul>';
           foreach ($arr as  $buttonX2 ){
             echo  $buttonX1 . $buttonX2 . $buttonX3;
           }
           echo '</ul>';  
        
        ?>
    </body>
</html>
