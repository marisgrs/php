<?php
session_start();
?>

    <!DOCTYPE html>
    <html>
    <?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (trim($_POST["submit"]) === 'Submit') {
        if (trim($_POST["answer"]) !== '') {
            array_push($_SESSION['your_answer'], trim($_POST["answer"]));
        } else {
            echo '!!! посторайся попасть по кнопке !!!';
        }
    }
}

if ((count($_SESSION['your_answer'])) >= (count($_SESSION['right_answer']))) {
    echo 'right answer / your answer';
    echo '</br>';
    for ($i = 0; $i < count($_SESSION['right_answer']); $i++) {

        if ($_SESSION['right_answer'][$i] == $_SESSION['your_answer'][$i]) {
            $ans = 'good';
        } else {
            $ans = 'bad';
        }
        echo $_SESSION['right_answer'][$i] . '/' . $_SESSION['your_answer'][$i] . ' : ' . $ans;
        echo '</br>';
    }

    session_unset();
    session_destroy();

    echo '<body>';
    echo '  <form action="/index.php" method="POST">';
    echo '      <input type="submit" name="repeat" value="Repeat">';
    echo '   </form>';

} else {

    echo '<body>';
    echo '  <form action="/action.php" method="POST">';
    echo '     Choose :';
    echo '     </br>';
    echo '      <input type="radio" name="answer" value="1"> Answer 1';
    echo '     </br>';
    echo '     <input type="radio" name="answer" value="2"> Answer 2';
    echo '     </br>';
    echo '      <input type="submit" name="submit" value="Submit">';
    echo '   </form>';

}
?>
        </body>

    </html>
