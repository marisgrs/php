DROP DATABASE IF EXISTS phpdb;
CREATE DATABASE phpdb;
USE phpdb;

CREATE TABLE catigories (id INT AUTO_INCREMENT NOT NULL, name VARCHAR (255) NOT NULL, PRIMARY KEY (id));
CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, login VARCHAR (255) NOT NULL, firstname VARCHAR (255) NOT NULL, lastname VARCHAR (255) NOT NULL, password VARCHAR (255) NOT NULL, PRIMARY KEY (id));
CREATE TABLE news (id INT AUTO_INCREMENT NOT NULL, name VARCHAR (255) NOT NULL, news_date DATETIME NOT NULL, news_text VARCHAR (255), catigories_id INT NOT NULL, user_id INT NOT NULL, PRIMARY KEY (id), FOREIGN KEY (catigories_id) REFERENCES catigories(id), FOREIGN KEY (user_id) REFERENCES user(id));



INSERT INTO user (user.login, user.firstname, user.lastname, user.password) VALUES ("user1", "firstname1", "secondname1", "pass");
INSERT INTO user (user.login, user.firstname, user.lastname, user.password) VALUES ("user2", "firstname2", "secondname2", "pass");
INSERT INTO user (user.login, user.firstname, user.lastname, user.password) VALUES ("user3", "firstname3", "secondname3", "pass");
INSERT INTO user (user.login, user.firstname, user.lastname, user.password) VALUES ("user4", "firstname4", "secondname4", "pass");
INSERT INTO user (user.login, user.firstname, user.lastname, user.password) VALUES ("user5", "firstname5", "secondname5", "pass");


INSERT INTO catigories (catigories.name) VALUES ("politics");
INSERT INTO catigories (catigories.name) VALUES ("culture");
INSERT INTO catigories (catigories.name) VALUES ("technology");
INSERT INTO catigories (catigories.name) VALUES ("finance");
INSERT INTO catigories (catigories.name) VALUES ("sportes");

INSERT INTO news (news.name, news.news_date, news.news_text, news.catigories_id, news.user_id ) VALUES ("news1", "2012-06-08 12:13:14", "text1" ,1, 1);
INSERT INTO news (news.name, news.news_date, news.news_text, news.catigories_id, news.user_id ) VALUES ("news2", "2012-06-08 12:13:14", "text2", 2, 2);
INSERT INTO news (news.name, news.news_date, news.news_text, news.catigories_id, news.user_id ) VALUES ("news3", "2012-06-08 12:13:14", "text3", 3, 3);
INSERT INTO news (news.name, news.news_date, news.news_text, news.catigories_id, news.user_id ) VALUES ("news4", "2012-06-08 12:13:14", "text4", 4, 4);
INSERT INTO news (news.name, news.news_date, news.news_text, news.catigories_id, news.user_id ) VALUES ("news5", "2012-06-08 12:13:14", "text5", 5, 5);
INSERT INTO news (news.name, news.news_date, news.news_text, news.catigories_id, news.user_id ) VALUES ("news6", "2012-06-08 12:13:14", "text6", 2, 5);
INSERT INTO news (news.name, news.news_date, news.news_text, news.catigories_id, news.user_id ) VALUES ("news7", "2012-06-08 12:13:14", "text6", 1, 5);
INSERT INTO news (news.name, news.news_date, news.news_text, news.catigories_id, news.user_id ) VALUES ("news8", "2012-06-08 12:13:14", "text6", 1, 3);

SELECT * FROM user;
SELECT * FROM catigories;
SELECT * FROM news;