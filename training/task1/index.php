<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <p>hello task1</p>
    <body>
        <?php
        /*------------------------------------------------*/
        echo '<br>Задача 1: Комментарии<br>';
        //Marys
        #Hrazhulis
        /*
          Marys
          Hrazhulis
         */
        echo 'hello from PHP';
        echo '<br>Задача 1: Комментарии<br>';
        $channelName;
        $addressManufacture;
        $colorCar;
        $waterDegree;
        $phoneModel;
        /*------------------------------------------------*/
        echo '<br>Задача 2: Именование переменных <br>';
        $num3 = 3;
        $num5 = 5;
        $num8 = 8;
        echo '$num3=' . $num3;
        echo '<br>';
        echo '$num5=' . $num5;
        echo '<br>';
        echo '$num8=' . $num8;
        echo '<br>';
        $sum = $num1 + $num2 + $num3;
        echo '$sum=' . $sum;
        /*------------------------------------------------*/
        echo '<br>Задача 3: Операторы <br>';
        $num4 = 4;
        $num6 = 6;
        $result = $num2 +  $num6 + ($num2/$num5) - $num1;
        echo '$result=' . $result;
        echo '<br>';
        $a = 1;
        $b = 2;
        echo '$a=' . $a;
        echo '<br>';
        echo '$b=' . $b;
        /*------------------------------------------------*/
        echo '<br>Задача 4: Копирование значений переменных и копирование переменных по ссылке <br>';
        $c = $a;
        $d = &$b;
        echo '$c=' . $c;
        echo '<br>';
        echo '$d=' . $d;
        echo '<br>';
        $a = 3;
        $b = 4;
        echo '$a=' . $a;
        echo '<br>';
        echo '$b=' . $b;
        echo '<br>';
        echo '$c=' . $c;
        echo '<br>';
        echo '$d=' . $d;
        /*------------------------------------------------*/
        echo '<br>Задача 5: Константы<br>';
        define("A", 41);
        define("B", 33);
        echo 'A+B=' . (A + B);
        echo '<br>';
        define("B", 313);
        echo 'A+B=' . (A + B);
        
        ?>
    </body>
</html>
