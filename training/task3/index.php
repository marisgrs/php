<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        /*------------------------------------------------*/
        echo '<br>Урок 6: Пользовательские функции  <br>';
        /*------------------------------------------------*/
        echo '<br>Задача 1: Информация о товарах в корзине  <br>';
        $products = array(
          array('name' => 'TVset', 'price' => '400', 'quantity' => 1),  
          array('name' => 'Phone', 'price' => '300', 'quantity' => 3),  
          array('name' => 'Sneackers', 'price' => '400', 'quantity' => 2),  
        );
        
        function result($array){
            $totalPrice = 0;
            $totalQuantity = 0;
            foreach ($array as $key) {
               $totalPrice += $key['price'] * $key['quantity'];
               $totalQuantity += $key['quantity'];
             }
             $result = array('price' => $totalPrice , 'quantity' => $totalQuantity);
            return $result;
        }
        print_r(result($products));
        /*------------------------------------------------*/
        echo '<br>Задача 2: Квадратное уравнение <br>';
        /*------------------------------------------------*/   
        function equation($a, $b, $c){
            $d = (pow($b, 2))-(4 * $a * $c);
                    if ($d > 0){
                        $x1 = (-$b + sqrt($d))/(2 * $a);
                        $x2 = (-$b - sqrt($d))/(2 * $a);
                        $result = array('x1' => $x1 , 'x2' => $x2);
                    } elseif ($d == 0 ) { 
                                $x = (-$b /(2 * $a));
                                $result = array('x' => $x); 
                    } else {
                             $x = 'false';
                             $result = array('x' => $x); 
                    }
                    return $result;   
                }
        print_r(equation(1, 2, 3));
       /*------------------------------------------------*/
        echo '<br>Задача 3: Удаление отрицательных элементов из массива (вариант 1)<br>';
        /*------------------------------------------------*/
        $digits = array(2,-10, -2, 4, 5, 1, 6, 200, 1.6, 95); 
        function deleteNegatives($array){
            $newArray = array();
            foreach ($array as $value) {
                if ($value >= 0){
                    array_push($newArray, $value); 
                }
            }
            return  $newArray;
        }
        print_r(deleteNegatives($digits));
        /*------------------------------------------------*/
        echo '<br>Решить задачу №3 используя передачу аргумента по ссылке. <br>';
       /*------------------------------------------------*/                
        function deleteNegativesLink(&$array){
            foreach ($array as $key => $value) {
                if ($value < 0){
                     unset($array[$key]);
                }
            }
            return  $array;
        }
        print_r(deleteNegativesLink($digits));
         /*------------------------------------------------*/
        echo '<br>Урок 8: HTTP, формы <br>';
        /*------------------------------------------------*/
        echo '<br>Задача 2: Использование форм #1  <br>';
        ?>
          <form action="/index.php" method="POST">
                1: <input type="text" name="1" value="" /> <br/>
                2: <input type="text" name="2" value="" /> <br/>
                3: <input type="text" name="3" value="" /> <br/>
                4: <input type="text" name="4" value="" /> <br/>
                5: <input type="text" name="5" value="" /> <br/>
                6: <input type="text" name="6" value="" /> <br/>
                7: <input type="text" name="7" value="" /> <br/>
                <input type="submit" name="submit" value="submit" />
            </form>
        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
             $number1 = toIntger($_POST["1"]);
             $number2 = toIntger($_POST["2"]);
             $number3 = toIntger($_POST["3"]);
             $number4 = toIntger($_POST["4"]);
             $number5 = toIntger($_POST["5"]);
             $number6 = toIntger($_POST["6"]);
             $number7 = toIntger($_POST["7"]);
        }
        function toIntger($text){
            return intval($text);
        }
        $resultPost = array(
            $number1,        
            $number2,        
            $number3,        
            $number4,        
            $number5,        
            $number6,        
            $number7,        
        );
        function avrg($array){
            $count = 0;
            foreach ($array as $value){
                $count += $value;
            }
            $avrg = $count/count($array);    
            return $avrg;
        }
        if ( count($resultPost) != 0){
            echo 'max: '. (max($resultPost) . "<br>");
            echo 'min: '. (min($resultPost) . "<br>");
            echo 'avrg:'. (avrg($resultPost) . "<br>");
        }
       /*------------------------------------------------*/
        echo '<br>Задача 3: Использование форм #2  <br>';
        /*------------------------------------------------*/
        ?>
          <form action="/index.php" method="POST">
                Name: <input type="text" name="name" value="" /> <br/>
                M: <input type="radio" name="gender" value="male" /> 
                F: <input type="radio" name="gender" value="female" /> <br/>
                <input type="submit" name="sumbit" value="submit" />
            </form>
       <?php
         if ($_SERVER["REQUEST_METHOD"] == "POST") {
             $name = trim($_POST["name"]);
             $gender = trim($_POST["gender"]);
        }
        define('MALE', 'male');
        define('FEMALE', 'female');
           function greeting($name, $gender){
               $greeting ;
               $welcom = 'Добро пожаловать,';
               $male = 'мистер';
               $female = 'миссис';
                if ($gender === MALE){
                    $greeting = $welcom .' '. $male . ' ' . $name . '!';
                }elseif($gender === FEMALE){
                    $greeting = $welcom . ' ' . $female . ' ' . $name . '!';
                }else{
                    $greeting = 'Hello anonymous !';
                }
                echo  $greeting;
        }
         greeting($name, $gender);
       ?>
    </body>
</html>
